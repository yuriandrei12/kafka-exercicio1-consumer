package br.com.mastertech.producer;

public class Acesso {

    private String nome;
    private String portaAcesso;
    private boolean temAcesso;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPortaAcesso() {
        return portaAcesso;
    }

    public void setPortaAcesso(String portaAcesso) {
        this.portaAcesso = portaAcesso;
    }

    public boolean isTemAcesso() {
        return temAcesso;
    }

    public void setTemAcesso(boolean temAcesso) {
        this.temAcesso = temAcesso;
    }
}