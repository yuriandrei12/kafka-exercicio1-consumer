package br.com.mastertech.consumer;

import br.com.mastertech.producer.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec2-yuri-andrei-1", groupId = "yuri-group")
    public void receber(@Payload Acesso acesso) throws IOException {
        System.out.println("Inicio");
        System.out.println("O " + acesso.getNome() + " acessou a porta " + acesso.getPortaAcesso());
        CSVUtil.gravaNoCSV(CSVUtil.convertToCSV(acesso));
        System.out.println("Fim");
    }

}